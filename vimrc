syntax on
filetype plugin indent on

runtime macros/matchit.vim

set autoindent
set backspace=indent,eol,start
set hidden
set incsearch
set ruler
set wildmenu
set confirm
set relativenumber

set expandtab
set shiftwidth=2
set softtabstop=2

nnoremap ; :
vnoremap ; :

let mapleader      = ' '
let maplocalleader = ','

nnoremap <leader>w <C-w>
nnoremap <leader>f gg=G``

"" Plugin configuration

" Airline
let g:airline#extensions#tabline#enabled = 1

" LSC
autocmd CompleteDone * silent! pclose

let g:lsc_auto_map = v:true
let g:lsc_server_commands = {}

"" Haskell

if executable('hie-wrapper')
  let g:lsc_server_commands.haskell = 'hie-wrapper --lsp'
endif

"" Rust

if executable('rls')
  let g:lsc_server_commands.rust = 'rls'
endif


if executable('rustfmt')
  au! FileType rust setlocal equalprg='rustfmt'
endif

"" C

if executable('clang-format')
  au! FileType c setlocal equalprg='clang-format'
endif

if executable('clangd')
  let g:lsc_server_commands.c = 'clangd --log=error'
endif

